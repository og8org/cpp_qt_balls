#include <QWidget>
#include <QtWidgets>
#include "Field.h"


Field::Field(float inlx, float inly, QWidget *parent=0): QWidget(parent), lx{inlx}, ly{inly} {
    dx = inlx/resx;
    dy = inly/resy;
    QTimer *tim = new QTimer;
    tim->setInterval(30);
    connect(tim,SIGNAL(timeout()),this,SLOT(drawMe()));
    tim->start();
}
    
void Field::paintEvent(QPaintEvent *){
    QPainter *painter = new QPainter(this);
    QPen pen;
    pen.setWidth(4);

    for(auto& thing: allThings) {
        Ball* ball = dynamic_cast<Ball*>(thing);
        //QRect rect(thing->x-thing->hx/2,thing->y-thing->hy/2,thing->hx,thing->hy);
        if (ball) {
            QRect rect(thing->x-thing->hx,thing->y-thing->hy,thing->hx*2,thing->hy*2);
            pen.setColor(QColor(50,50,50));
            painter->setBrush(ball->getcolor());
            painter->drawEllipse(rect);
        }
        else { // wall
            QRect rect(thing->x-thing->hx/2,thing->y-thing->hy/2,thing->hx,thing->hy);
            pen.setColor(QColor(100,100,100));
            painter->setBrush(QColor(150,200,200));
            painter->drawRect(rect);
        }
    }

    painter->end();
}

void Field::colorAll() {
    std::vector<Thing *> colluding;
    float posx, posy;
    std::vector<Ball *> collBalls;
    std::vector<Wall *> collWalls;
    for(int iy=0; iy<resy; iy++) {
        posy = iy*dy;
        for(int ix=0; ix<resx; ix++) {
            posx = ix*dx;
            //
            colluding.clear();
            for(const auto& thing: allThings) {
                // push all things on that position to colluding vector
                if (thing->isHere(posx, posy)) {
                    colluding.push_back(thing);
                }
            }
            if (colluding.size() == 0)
                mesh[ix][iy] = NULL;
            else if (colluding.size() == 1)
                mesh[ix][iy] = colluding[0];
            else { // collusion of many objects
                collBalls.clear();
                collWalls.clear();
                for(const auto& coll: colluding) {
                    Ball* ball = dynamic_cast<Ball*>(coll);
                    if (ball)
                        collBalls.push_back(ball);
                    else {
                        Wall* wall = dynamic_cast<Wall*>(coll);
                        collWalls.push_back(wall);
                    }
                }
                if (collBalls.empty()) { // just walls on top of each other
                    mesh[ix][iy] = colluding[0];
                }
                else { // collBalls change speed direction
                    if (collBalls.size() == 2) {
                      collBalls[0]->invertSpeed(collBalls[1]->getvx(), collBalls[1]->getvy());
                      collBalls[1]->invertSpeed(collBalls[0]->getvx(), collBalls[0]->getvy());
                    }
                    else {
                        for(auto& ball: collBalls) {
                            ball->invertSpeed(collWalls[0]->nx, collWalls[0]->ny);
                        }
                    }
                }
            }
        }
    }
};

void Field::drawMe() {
    //std::cout << "." << std::flush;
    for(const auto& thing: allThings) {
        Ball* ball = dynamic_cast<Ball*>(thing);
        if (ball) {
            ball->move();
        }
    }
    colorAll();
    update();
};
