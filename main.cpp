#include <QApplication>
#include "Field.h"

#include <iostream>
#include <vector>
#include <unistd.h> // sleep
#include <time.h>



int main(int argc, char *argv[])
{
    const int NUM_BALLS = 6;
    const int NUM_WALLS = 4;
    const float WALL_SIZE = 8.0;
    const float ZERO = WALL_SIZE;
    const float FIELDX = 500.0;
    const float FIELDY = 300.0;

    std::cout << " === START ===\n";

    srand(time(NULL));

    // init field
    //Field field{FIELDX, FIELDY};
    QApplication a(argc, argv);
    Field field{FIELDX, FIELDY, NULL};
    field.show();
    // 4 walls at all the sides
    Wall walls[NUM_WALLS]{  {FIELDX/2, ZERO, FIELDX-WALL_SIZE, WALL_SIZE, 0, 1},
                            {FIELDX/2, FIELDY-ZERO, FIELDX-WALL_SIZE, WALL_SIZE, 0, -1},
                            {ZERO, FIELDY/2, WALL_SIZE, FIELDY-WALL_SIZE, 1, 0},
                            {FIELDX-ZERO, FIELDY/2, WALL_SIZE, FIELDY-WALL_SIZE, -1, 0} };
    for(auto& wall: walls) { field.addObj(&wall); }
    // init balls
    //Ball balls[NUM_BALLS]{ {170, 133, 20.0}, {43, 51, 20.0}, {70, 33, 10.0}, {470, 233, 10.0} };
    Ball balls[NUM_BALLS];
    for(auto& ball: balls) { field.addObj(&ball); }
    std::cout << " Things in field : " << field.ThingSize() << "\n";
    std::cout << " Balls in field : " << NUM_BALLS << "\n";
    std::cout << " Print field. \n";

    // set field
    //field.colorAll();
    //
    // print field
    //field.drawMe();
    //
    // give ball a speed
    //balls[0].kick(7.0, 1.0);
    //balls[1].kick(-2.8, 9.0);
    //balls[2].kick(1.0, 2.0);
    //balls[3].kick(-2.8, -9.0);
    while(false) {
        // move balls
        for(auto& ball: balls) { ball.move(); }
        //field.colorAll();
        field.drawMe();
    }   

    return a.exec();
}
