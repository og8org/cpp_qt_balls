#ifndef FIELD_H
#define FIELD_H

#include <QWidget>

#include <iostream>
#include <vector>
#include <cmath>
#include <stdlib.h>


class Thing {
    public:
        float x, y;                               // center pos
        float hx, hy;                             // width/2, height/2
        static constexpr float dt = 1.0;          // time of one timestep, better global?
        Thing(float inx, float iny):
            x(inx),
            y(iny) {};
        virtual bool isHere(float testx, float testy) = 0;
};

class Wall: public Thing {
    public:
        int nx, ny;   // norm of area
        Wall(float inx, float iny, float inhx, float inhy, int innx, int inny): Thing(inx, iny) { hx = inhx; hy = inhy; nx = innx; ny = inny; };
        virtual bool isHere(float testx, float testy) {
            return (x-hx/2 <= testx && testx <= x+hx/2 && y-hy/2 <= testy && testy <= y+hy/2);
        }
};

class Ball: public Thing {
    public:
        Ball(float inx, float iny, float inr=10.0): Thing(inx, iny), r(inr) {
            hx = inr;
            hy = inr;
            color = QColor(rand()%255,rand()%255,rand()%255);
        };
        Ball(): Thing(10+rand()%450, 20+rand()%250) {
            // all random
            r = 4.0 + rand()%20;
            hx = r;
            hy = r;
            kick(rand()%12, rand()%12);
            color = QColor(rand()%255,rand()%255,rand()%255);
            std::cout << x << " - " << y << " - " << hx << " - " << hy << "\n" << std::flush;
        };
        virtual bool isHere(float testx, float testy) {
            return (std::pow(testx - x,2) + std::pow(testy - y,2) <= std::pow(r,2));
        }
        void kick(float invx, float invy = 0.0) { vx = invx; vy = invy; };
        void move() {
            x += vx*dt;
            y += vy*dt;
        };
        float getvx() { return vx; };
        float getvy() { return vy; };
        QColor getcolor() { return color; };
        virtual void invertSpeed(const int nx, const int ny) {
            if (std::pow(memx-x,2) + std::pow(memy-y,2) < r*r*2)
                return; // prevent double collusion
            std::cout << "BANG! \n" << std::flush;
            if (std::abs(nx) == 1)
                vx = -vx;
            else if (std::abs(ny) == 1)
                vy = -vy;
            memx = x;
            memy = y;
        };
    private:
        float vx, vy;       // speed
        float r;            // radius
        float memx, memy;   // remember last collusion point
        QColor color;
};

class Field : public QWidget {
    Q_OBJECT
    public:
        Field(float inlx, float inly, QWidget *parent);
        void colorAll();
        void addObj(Thing * thing) { allThings.push_back(thing); };
        int ThingSize() const { return allThings.size(); };
        float getlx() { return lx; };
        float getly() { return ly; };
    private:
        static const int resx = 800, resy = 400;
        float lx, ly;                   // total dimensions
        float dx, dy;                   // step size
        Thing * mesh[resx][resy];       // what covers field part
        std::vector<Thing *> allThings; // save all things on field
    public slots :
        void drawMe();
    protected :
        void paintEvent(QPaintEvent *);
};

#endif
